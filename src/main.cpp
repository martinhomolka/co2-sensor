#include <Arduino.h>
#include "SoftwareSerial.h"
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>
#include <MHZ19.h>

// CYCLE DEFINITION
#define SENSOR_CYCLE_INIT 0
#define SENSOR_CYCLE_SETUP 1
#define SENSOR_CYCLE_MEASURE 2
#define SENSOR_CYCLE_MEASURING 3
#define SENSOR_CYCLE_PUBLISHING 4
#define SENSOR_CYCLE_SLEEP 5

// MQTT SETTINGS
#define MQTT_HOST IPAddress(192, 168, 88, 139)
#define MQTT_PORT 1883
#define MQTT_TOPIC_PATH "sensors/livingroom/carbon dioxide/"
#define MQTT_TOPIC_VALUE_PATH "sensors/livingroom/carbon dioxide/carbon dioxide level"
#define MQTT_TOPIC_LEVEL_PATH "sensors/livingroom/carbon dioxide/carbon dioxide detected"
#define MQTT_TOPIC_THRESHOLD "sensors/livingroom/carbon dioxide/carbon dioxide threshold"
#define MQTT_TOPIC_TEMPERATURE "sensors/livingroom/carbon dioxide/temperature"
#define MQTT_TOPIC_FAULT "sensors/livingroom/carbon dioxide/fault"
#define MQTT_TOPIC_MEASUREMENT_INTERVAL "sensors/livingroom/carbon dioxide/measurement interval"

// CO2 LEVELS
#define CO2_LEVEL_NORMAL "NORMAL"
#define CO2_LEVEL_ABNORMAL "ABNORMAL"

#define SYSTEM_STATUS_FAULT 1
#define SYSTEM_STATUS_OK 0

int status = WL_IDLE_STATUS;     // the Wifi radio's status
AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

SoftwareSerial softwareSerial(D7,D8);
MHZ19 mhz19(&softwareSerial);

int co2 = 0;
int co2Threshold = 0;
float temperature = 0;

Ticker co2Timer;
Ticker lightTimer;
Ticker sleepTimer;
int measurementInterval = 0; // in sec

int fault = 0;
int sensorCycle = 0;

boolean isSetupFinished() {
  if (co2Threshold > 0 && measurementInterval > 0) {
    return true;
  } else {
    return false;
  }
}

void setSensorCycle(int cycle) {
  sensorCycle = cycle;
}

int getSensorCycle() {
  return sensorCycle;
}

void turnLightOn() {
  digitalWrite(LED_BUILTIN, LOW);
}

void turnLightOff() {
  digitalWrite(LED_BUILTIN, HIGH);
}

void blinkLight() {
  turnLightOn();
  lightTimer.detach();
  lightTimer.once_ms(500,turnLightOff);
}

void blinkLight(int ms) {
  turnLightOn();
  lightTimer.detach();
  lightTimer.once_ms(ms,turnLightOff);
}

void blinkLight(int ms, int times) { // TODO, does not work
  int i = 0;
  while (i < times) {
    turnLightOn();
    delay(ms);
    turnLightOff();
    delay(ms);
    i++;  
  }
}

void setFault(int value) {
  if (value == SYSTEM_STATUS_FAULT or value == SYSTEM_STATUS_OK) {
    if (value != fault) {
      fault = value;
      if (mqttClient.connected()) {
        char faultChar[2];
        String(fault).toCharArray(faultChar,2);
        mqttClient.publish(MQTT_TOPIC_FAULT, 1, true, faultChar);
      }
    }
  }
}

void publishCO2Level(int co2) {
  if (mqttClient.connected()) {
    Serial.println("Publishing new payload to MQTT");
    char co2char[6];
    String(co2).toCharArray(co2char,6);
    mqttClient.publish(MQTT_TOPIC_VALUE_PATH, 1, true, co2char);
    if (co2 >= co2Threshold) {
      mqttClient.publish(MQTT_TOPIC_LEVEL_PATH, 1, true, CO2_LEVEL_ABNORMAL);
    } else {  
      mqttClient.publish(MQTT_TOPIC_LEVEL_PATH, 1, true, CO2_LEVEL_NORMAL);
    }
    setSensorCycle(SENSOR_CYCLE_SLEEP);
    // allowToSleep = true;
  }
}

void measureCo2() {
  blinkLight();
  MHZ19_RESULT response = mhz19.retrieveData();
  if (response != MHZ19_RESULT_OK) {
    co2 = 0;
    Serial.printf("MHZ19 error: %d\n", response);
    setFault(SYSTEM_STATUS_FAULT);
  } else {
    co2 = mhz19.getCO2();
    Serial.printf("MHZ19: CO2 %dppm, temperature %d˚C\n", co2, mhz19.getTemperature());
    publishCO2Level(co2);
    setFault(SYSTEM_STATUS_OK);
  }
}

void connectToWifi() {
  Serial.println("Connecting to Wi-Fi...");
  WiFi.hostname(WIFI_HOSTNAME);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected& event) {
  Serial.println("Disconnected from Wi-Fi.");
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
}

void onWifiConnect(const WiFiEventStationModeGotIP& event) {
  Serial.println("Connected to Wi-Fi.");
  connectToMqtt();
}

void onMqttConnect(bool sessionPresent) {
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);

  Serial.printf("Subscribing to topic \"%s\"\n",MQTT_TOPIC_THRESHOLD);
  mqttClient.subscribe(MQTT_TOPIC_THRESHOLD,1);
  Serial.printf("Subscribing to topic \"%s\"\n",MQTT_TOPIC_MEASUREMENT_INTERVAL);
  mqttClient.subscribe(MQTT_TOPIC_MEASUREMENT_INTERVAL,1);

  // measureCo2();
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTT.");

  if (WiFi.isConnected()) {
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("Subscribe acknowledged.");
}

void onMqttUnsubscribe(uint16_t packetId) {
  Serial.println("Unsubscribe acknowledged.");
}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  blinkLight(200);
  Serial.println("Publish received.");
  Serial.print("  topic: ");
  Serial.println(topic);

  if (strcmp(topic,MQTT_TOPIC_THRESHOLD) == 0) {
    int newThreshold = atoi(payload);
    if (newThreshold > 0) {
      Serial.printf("Setting new CO2 threshold to %d\n",newThreshold);
      co2Threshold = newThreshold;
    }
  } else if (strcmp(topic,MQTT_TOPIC_MEASUREMENT_INTERVAL) == 0) {
    int newMeasurementInterval = atoi(payload);
    if (newMeasurementInterval > 0) {
      Serial.printf("Setting new measurement interval to %d s\n",newMeasurementInterval);
      measurementInterval = newMeasurementInterval;
      // co2Timer.detach();
      // co2Timer.attach(measurementInterval, measureCo2);
    }
  }
  if (isSetupFinished()) {
    setSensorCycle(SENSOR_CYCLE_MEASURE);
  }
}

void onMqttPublish(uint16_t packetId) {
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void goToSleep() {
  Serial.printf("Going to sleep for %d s. ZzZzZzz...\n",measurementInterval);
  ESP.deepSleep(measurementInterval * 1000000);
}

void setup() {
  // put your setup code here, to run once:
  setSensorCycle(SENSOR_CYCLE_INIT);

  pinMode(LED_BUILTIN, OUTPUT);
  turnLightOn();
  
  Serial.begin(9600);
  while (!Serial) {
    ;
  }
  Serial.println("Project setup started");
  
  // softwareSerial.begin(9600, SWSERIAL_8N1, D8, D7, false, 256);
  softwareSerial.begin(9600);
  mhz19.setAutoCalibration(false);

  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.setCredentials(MQTT_USERNAME, MQTT_PASSWORD);
  turnLightOff();

  connectToWifi();

  setSensorCycle(SENSOR_CYCLE_SETUP);

}

void loop() {
  // put your main code here, to run repeatedly:
  if (getSensorCycle() == SENSOR_CYCLE_MEASURE) {
    setSensorCycle(SENSOR_CYCLE_MEASURING);
    measureCo2();
  } else if (getSensorCycle() == SENSOR_CYCLE_SLEEP) {
    goToSleep();
  }
}